const http = require("http"); // Import HTTP library
var controller = require('./controller/controller'); // Import the controller file for the functions

const port = 8080; // Define port to be used by the server

const server = http.createServer(function (req, res) {
    // Separate function in the controller.js file
    if (req.method == "POST") {
        // If method is POST, then process potential payload
        controller.processPayload(req, res);
    } else {
        // If the endpoint doesn't exist or the method is incorrect
        controller.invalidURL(req, res);
    }
});

// Catch any errors with starting the server
try {
    server.listen(port);
    console.log("Server is litening on port " + port);
} catch (error) {
    console.error("There was an issue starting the server on " + port + ": " + error);
}