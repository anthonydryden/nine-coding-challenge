exports.processPayload = function(req, res) {
    var body = '';

    // Collect the body of the POST request
    req.on('data', function (chunk) {
        body += chunk;
    });

    req.on('end', function () {
        var matchedShowDetails = []; //Store matched show details in a standard array
        var errorJson = '';
            try {
            //Convert the body into a JSON variable           
            var receivedJson = JSON.parse(body);

                try {
                    //Run for the length of the payload array
                    for (i = 0; i < receivedJson.payload.length; i++) {
                        //Check two fields, DRM and at least some episodes
                        if (receivedJson.payload[i].drm == true && receivedJson.payload[i].episodeCount > 0) {
                            //Push the matches to the array and expand the size
                            matchedShowDetails.push(
                                {
                                    image: receivedJson.payload[i].image.showImage,
                                    slug: receivedJson.payload[i].slug,
                                    title: receivedJson.payload[i].title
                                }
                            );
                        }
                    }

                    //Place the matchedShowDetails array into a cleaner response JSON object
                    var responseJson = {response:matchedShowDetails}
                    
                    //Send the response as a string with some prettying
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.end(JSON.stringify(responseJson));
                } catch (error) {
                    // Catch any issues with JSON being valid but not to specifications
                    console.error("Invalid JSON provided: " + error);
                    errorJson = 
                    {
                        error: "Could not decode request: " + error
                    }
                    res.statusCode = '400';
                res.setHeader('Content-Type', 'application/json');
                res.end(JSON.stringify(errorJson));
                }
            } catch (error) {
                // Catch any errors with invalid JSON being provided
                console.error("Invalid JSON provided: " + error);
                errorJson = 
                {
                    error: "Could not decode request: " + error
                }

                res.statusCode = '400';
                res.setHeader('Content-Type', 'application/json');
                res.end(JSON.stringify(errorJson));
            }
        });
}

exports.invalidURL = function (req, res) {
    // Provide response if there is an invalid request posted
    
    var errorJson = 
    {
        error: "Could not decode request: Invalid endpoint"
    }

    res.statusCode = 404;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(errorJson));
}