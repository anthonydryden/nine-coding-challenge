# README #

## What is this repository for? ##

* This is a RESTful API built by Anthony Dryden for an application.
* v1.0.0

## How do I get set up? ##

* [Summary of set up](#summary-of-set-up)
* [Configuration](#configuration)
* [Dependencies](#dependencies)
* [How to run tests](#how-to-run-tests)

## Design Considerations ##

* [Why did I choose this language/these libraries?](#why-did-i-choose-this-languagethese-libraries)
* [How did I test the application?](#how-did-i-test-the-application)

## Resources Used ##
* [URL Resources Used](#url-resources-used)

## How do I get set up? ###

### Summary of set up
The application itself is set up using `app.js` and hosted on AWS. The application handles a single endpoint, which is `POST` in which is searches a payload and grabs three fields from the submitted `JSON` body data. These fields are `image`, `slug`, and `title`.

### Configuration
This application uses vanilla JavaScipt and NodeJS. The application uses `HTTP` requests and handles `POST` requests and provides an error message for any other request methods, or any `POST` methods not including valid JSON.   

### Dependencies
This application uses vanilla JavaScript and only built in NodeJS modules. No other `npm` installations or libraries are required.  

### How to run tests
The application is hosted on AWS at the following link: http://ninecodingchallenge.ap-southeast-2.elasticbeanstalk.com/
I have opted to use the free installed client of [Postman](https://www.postman.com/) to specify the request method and request body in the testing process.
Under the `resources` folder, you'll find some requests that I have built and was using to test before learning about Postman. I have left them in for clarity's sake and because I have worked on them. 
The app listens on port **8080**.

## Design Considerations ##

### Why did I choose this language/these libraries?
I have the most experience with `Java` and `JavaScript` throughout my time at university and working on independent projects, and JavaScript is an extension of this knowledge. I understand that `NodeJS` is still used in the stack of the business and it has been wonderful to learn more and develop my skills while working on this challenge. I opted to not use any specific extra libraries or modules as I believe it is the best way to learn and fully get in touch with the language and learn the quirks. If I am able to code in vanilla JavaScript, then I can fully understand what the libraries provide and how they assist the process and why they are needed.  

### How did I test the application?
I have tested the application using the free installed client of [Postman](https://www.postman.com/) and tested the application locally using the `http://localhost:8080` address. Further testing was done once the solution was hosted on AWS and it has passed all tests and provided the correct response in both success and error. I had also built other scripts which I would run and use to test so I could fully understand the `POST` process, but once I had found Postman, these scripts were no longer needed, but can be found under the `resources` folder for completeness.

---------------------------------------

## Resources Used ##

### URL Resources Used
I used the following URL resouces and guides to assist with my code:  
- A guide on how to initially set up the project:
    * https://morayodeji.medium.com/building-nodejs-api-without-expressjs-or-any-other-framework-977e8768abb1
- Getting the body of data from a POST request:
    * https://nodejs.dev/learn/get-http-request-body-data-using-nodejs
- List of all returnable status codes:
    * https://restfulapi.net/http-status-codes/
- Converting JSON to and from string, and prettifying:
    * https://codesamplez.com/programming/using-json-in-node-js-javascript
- Pushing to an array:
    * https://stackoverflow.com/questions/36856232/write-add-data-in-json-file-using-node-js
- Error with application sending responses as string:
    * https://www.codegrepper.com/code-examples/javascript/node+res.end+json
