const http = require('http');
const sampleJson = require('../sample_request.json');

const options = {
    hostname: 'localhost',
    port: 8080,
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
};

const postrequest = http.request(options, response => {
    let data = "";
     
    response.on("data", chunk => {
        data += chunk;
    });

    response.on("end", () => {
        console.log(data);
    });
});


postrequest.on("error", error => {
    console.error(error);
});
postrequest.write(JSON.stringify(sampleJson));
postrequest.end();